import Vue from 'vue'
import Vuex from 'vuex'

import list from './modules/list'
import gallery from './modules/gallery' 

Vue.use(Vuex)




export default new Vuex.Store({
  modules:{
    gallery,
    list
  }
})
