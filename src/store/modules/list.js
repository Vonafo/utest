export default {
    state: {
        posts: []
      },
      mutations: {
        updatePostArr(state, posts) {
          state.posts = posts
        }
      },
      actions: {
        async fetchList(ctx, limit = 5) {
          const res = await fetch('https://jsonplaceholder.typicode.com/photos?_limit=' + limit);
          const posts = await res.json();
          ctx.commit('updatePostArr', posts)
        }
      },
      getters: {
        validPosts(state) {
          return state.posts
        }
      }
}