var client_id = '51612d6d9e4f4f6edd41cad56aad2fff68f0720456bc81e2ebcde02a6bf08de5';

export default {
    state: {
        imgList: [],
    },
    mutations: {
        updateImgList(state, imgList) {
            state.imgList = imgList.results
        }
    },
    actions: {
        async fetchImg(ctx, query = 'car') {
            const res = await fetch('https://api.unsplash.com/search/photos?query=' + query + '&client_id=' + client_id);
            const imageList = await res.json();
            ctx.commit('updateImgList', imageList);
        }
    },
    getters: {
        validImageList(state) {
            return state.imgList
        }
    }
}